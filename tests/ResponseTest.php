<?php
/**
 * @author Mateusz Aniołek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard Sp. Z o.o.
 * @homepage http://vegas-cmf.github.io
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TokenizerPhp\Tests;

include_once 'Mocks/ConnectorMock.php';

use TokenizerPhp\Tests\Mocks\Tokenizer\ConnectorMock;
use TokenizerPhp\Tokenizer\Response;

class ResponseTest extends \PHPUnit_Framework_TestCase
{

    public function setUp() { }

    public function configDataProvider()
    {
        return array(
            array('0', '1234567890', 'http://fake.response.url.com/authorize/')
        );
    }

    /**
     * @expectedException \TokenizerPhp\Tokenizer\Exception
     */
    public function testInvalidParam()
    {
        $response = Response::parse('config',
            json_encode(array(
                'status_code' => 400,
                'error_messages' => 'fail response'
            ))
        );

        $this->assertInstanceOf('\TokenizerPhp\Tokenizer\Response\Config', $response);

    }

    /**
     * @expectedException \TokenizerPhp\Tokenizer\Exception
     */
    public function testInvalidType()
    {
        $response = Response::parse('invalid_type',
            json_encode(array(
                'key' => 'value'
            ))
        );

        $this->assertInstanceOf('\TokenizerPhp\Tokenizer\Response\Config', $response);

    }

    public function testConfig()
    {
        $response = Response::parse('config',
            json_encode(array(
                'key' => 'value'
            ))
        );

        $this->assertInstanceOf('\TokenizerPhp\Tokenizer\Response\Config', $response);

    }

    public function testVerify()
    {
        $response = Response::parse('verify',
            json_encode(array(
                'key' => 'value'
            ))
        );

        $this->assertInstanceOf('\TokenizerPhp\Tokenizer\Response\Verify', $response);

    }

    public function testCreate()
    {
        $response = Response::parse('create',
            json_encode(array(
                'key' => 'value'
            ))
        );

        $this->assertInstanceOf('\TokenizerPhp\Tokenizer\Response\Create', $response);

    }


}