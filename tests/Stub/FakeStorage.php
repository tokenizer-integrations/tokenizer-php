<?php
/**
 * @author Mateusz Aniołek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard Sp. Z o.o.
 * @homepage http://vegas-cmf.github.io
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TokenizerPhp\Tests\Stub;

use TokenizerPhp\Tokenizer\Storage\SessionStorageInterface;

class FakeStorage implements SessionStorageInterface {

    private $fakeStorage = null;

    /**
     * Get key from storage
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return (isset($this->fakeStorage[$key]) ? $this->fakeStorage[$key] : null);
    }

    /**
     * Set value on given key
     * @param $key
     * @param $value
     * @return mixed
     */
    public function set($key, $value)
    {
        $this->fakeStorage[$key] = $value;
    }
}