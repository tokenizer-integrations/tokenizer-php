<?php
/**
 * Exception Class
 *
 * @author Mateusz Aniolek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard
 */

namespace TokenizerPhp\Tokenizer\Storage;

class Exception extends \TokenizerPhp\Tokenizer\Exception {
    
}

