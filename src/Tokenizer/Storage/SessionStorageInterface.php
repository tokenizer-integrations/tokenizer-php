<?php
/**
 * StorageInterface Class
 *
 * @author Mateusz Aniolek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard
 */

namespace TokenizerPhp\Tokenizer\Storage;

interface SessionStorageInterface
{
    /**
     * Get key from storage
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * Set value on given key
     * @param $key
     * @param $value
     * @return mixed
     */
    public function set($key, $value);

} 